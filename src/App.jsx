import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'

function App() {
  const [count, setCount] = useState(0)

  return (
    <div className="App">
      <a href='https://www.kairosds.com/es/index.html' >
        <img className='kairos-img' src='/kairos.jpeg'></img>
      </a>
      <h1>Trabajando con Docker</h1>
      <p>Brais Suárez López</p>
    </div>
  )
}

export default App
